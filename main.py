import tkinter

window = tkinter.Tk()
window.title("BMI Calculator")
window.config(padx=20, pady=20)
window.minsize(width='300',height='200')

def bmi():
    height = height_input.get()
    weight = weight_input.get()
    if weight == "" or height == "":
        result_label.config(text="Enter both weight and height!")
    else:
        try:
            bmi = float(weight) / ((float(height) / 100) ** 2)
            result_string = write_result(bmi)
            result_label.config(text=result_string)
        except:
            result_label.config(text="Enter a valid number!")


weight_label = tkinter.Label(text="Enter Your Weight (kg)")
weight_label.pack()

weight_input = tkinter.Entry(width=5, justify='center')
weight_input.pack()

height_label = tkinter.Label(text="Enter Your Height (cm)")
height_label.pack()

height_input = tkinter.Entry(width=5, justify='center')
height_input.pack()

calculate_button = tkinter.Button(text="Calculate", command=bmi)
calculate_button.config(pady=5, padx=3)
calculate_button.pack()

result_label = tkinter.Label()
result_label.config(pady=10)
result_label.pack()

def write_result(bmi):
    result_string = f"Your BMI is {round(bmi, 2)}. You are "
    if bmi <= 16:
        result_string += "severely thin!"
    elif 16 < bmi <= 17:
        result_string += "moderately thin!"
    elif 17 < bmi <= 18.5:
        result_string += "mild thin!"
    elif 18.5 < bmi <= 25:
        result_string += "normal weight"
    elif 25 < bmi <= 30:
        result_string += "overweight"
    elif 30 < bmi <= 35:
        result_string += "obese class 1"
    elif 35 < bmi <= 40:
        result_string += "obese class 2"
    else:
        result_string += "obese class 3"
    return result_string


window.mainloop()